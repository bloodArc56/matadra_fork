package mataDra.entity;

public abstract class Entity {

	private int index;


    /**コンストラクタ自動作成
     * @param index
     */
    public Entity(int index) {
        this.index = index;
    }
    //ゲッターセッター自動生成
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

}
