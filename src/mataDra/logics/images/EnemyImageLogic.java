package mataDra.logics.images;

import java.util.List;

import mataDra.dao.images.EnemyImageDAO;
import mataDra.entity.images.EnemyImageEntity;
import mataDra.logics.DAOLogic;

public class EnemyImageLogic extends DAOLogic<EnemyImageDAO> {

	// コンストラクタ
	public EnemyImageLogic() {
		// 登録したデータを読み込む
		setDao(new EnemyImageDAO());
		// データの数を記録する
		setCount(getDao().registerAll().size());

	}

	public String imageTop(int index) {

		// 敵キャラ上部を表示
		return getDao().findByIndex(index).getImageTop();
	}

	public void showBottom(int index) {
		// 敵キャラ上部を表示
		System.out.print(getDao().findByIndex(index).getImageBottom());
	}

	/**
	 * 敵1体表示
	 *
	 * @param index
	 */
	public String exec(List<Integer> groupIndex) {
		int count = groupIndex.size();
		String text = "";

		// 敵キャラ上部を格納
		for (int i = 0; i < count; i++) {

			EnemyImageEntity enemyImageEntity = getDao().findByIndex(groupIndex.get(i));
			text += enemyImageEntity.getImageTop();
		}
		text += "\n";

		// 敵キャラ下部を格納
		for (int i = 0; i < count; i++) {

			EnemyImageEntity enemyImageEntity = getDao().findByIndex(groupIndex.get(i));
			text += enemyImageEntity.getImageBottom();
		}
		text += "\n";


		return text;

	}

	/**
	 * 敵複数体表示
	 *
	 * @param enemiesIndex
	 */
	public void showGroup(List<Integer> enemiesIndex) {

		// 敵キャラ上部を表示
		for (int index : enemiesIndex) {
			imageTop(index);
		}
		System.out.print("\n");
		// 敵キャラ下部を表示
		for (int index : enemiesIndex) {
			showBottom(index);
		}
		System.out.println();
	}

	@Override
	public String convert(int index) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
}
