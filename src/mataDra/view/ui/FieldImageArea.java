package mataDra.view.ui;

import mataDra.logics.images.FieldImageLogic;

/**フィールド表示エリアを作成
 * @author ken4310
 *
 */
public class FieldImageArea {
    public void show(int index){
        FieldImageLogic fieldImageLogic = new FieldImageLogic();
        String text = fieldImageLogic.exec(index);
        System.out.println(text);

    }
}
