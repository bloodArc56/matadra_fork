package mataDra.dao.ability;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.ability.AbilityEntity;
import mataDra.entity.ability.AbilityEntity.AbilityType;

public class AbilityDAO extends DAO<AbilityEntity> {

	@Override
	public List<T> registerAll() {
		// TODO 自動生成されたメソッド・スタブ
		setEntity(new AbilityEntity(0, "オーラシュート", "敵全体に2倍ダメージ", "くらいなさい", 2, 0, 0, 3, AbilityType.DOUBLING));
		setEntity(new AbilityEntity(0, "たつまき", "敵全体に500ダメージ", "こいつでどうだ", 500, 0, 0, 3, AbilityType.ATTACK));
	}

}
